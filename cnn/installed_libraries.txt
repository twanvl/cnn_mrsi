Brian2==2.0b3
Cython==0.23.2
Django==1.8.4
Jinja2==2.8
Lasagne==0.2.dev1
MarkupSafe==0.23
PIL==1.1.7
Pillow==2.9.0
PyYAML==3.11
Pygments==2.0.2
Theano==0.7.0
argcomplete==1.0.0
argparse==1.2.1
backports.ssl-match-hostname==3.4.0.2
blocks==0.0.1
brian==1.4.1
certifi==2015.4.28
decorator==4.0.2
distribute==0.6.24
dtw==1.0
fuel==0.0.1
functools32==3.2.3.post2
h5py==2.5.0
image==1.4.1
ipykernel==4.0.3
ipython==4.0.0
ipython-genutils==0.1.0
jsonschema==2.5.1
jupyter-client==4.0.0
jupyter-core==4.0.4
matlabengineforpython==R2015a
matplotlib==1.4.3
minepy==1.0.0
mistune==0.7.1
mlxtend==0.3.0
mock==1.0.1
nbconvert==4.0.0
nbformat==4.0.0
nolearn==0.6a0.dev0
nose==1.3.7
notebook==4.0.4
numexpr==2.4.3
numpy==1.9.2
pandas==0.16.2
path.py==8.1
pexpect==3.3
picklable-itertools==0.1.1
pickleshare==0.5
progressbar2==2.7.3
ptyprocess==0.5
pyparsing==2.0.3
python-dateutil==2.4.2
pytz==2015.4
pyzmq==14.7.0
readline==6.2.4.1
requests==2.7.0
scikit-learn==0.15.2
scikits.talkbox==0.2.5
scipy==0.16.0
simplegeneric==0.8.1
six==1.9.0
spectral==0.2.0
sympy==0.7.6
tables==3.2.1.1
tabulate==0.7.5
terminado==0.5
toolz==0.7.4
tornado==4.2.1
traitlets==4.0.0
wsgiref==0.1.2
