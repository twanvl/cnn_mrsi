#!/usr/bin/env bash
# Pass pip extra options as script arguments
XOPTS=''
if [[ "$#" > "0" ]]; then
    XOPTS="${@:1}"
fi

if [[ -z "${VIRTUAL_ENV}" ]]; then
  echo Please run in a virtual environment:
  echo source venv/bin/activate
  exit
fi

PIP=pip3

# Change to script directory
#echo "Change to script directory" 
cd "$(dirname "$0")"

# Install required packages from online repositories and
# from local directory for nolearn
echo "Install required packages from online repositories and from local directory for nolearn"
${PIP} install https://github.com/Lasagne/Lasagne/archive/master.zip
${PIP} install ${XOPTS} -r cnn/requirements.txt
${PIP} install ${XOPTS} -e cnn/src/nolearn

# Change to original directory
echo "DONE. Change back to the original directory"
cd -
#exit 0
